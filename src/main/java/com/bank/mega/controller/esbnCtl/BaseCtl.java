package com.bank.mega.controller.esbnCtl;


import java.util.Arrays;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.bank.mega.UltimateBase;
import com.bank.mega.config.HttpRestResponse;
import com.bank.mega.config.ParamQueryCustomLib;
import com.google.gson.Gson;


@Component
public class BaseCtl extends UltimateBase{

	@SuppressWarnings("rawtypes")
	protected HttpRestResponse wsBody(String url, Object body, HttpMethod method, Map<String, String> headerMap,
			ParamQueryCustomLib... paramQuery) {
		MultiValueMap<String, Object> header = new LinkedMultiValueMap<>();
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));

		if (headerMap != null) {
			for (Entry<String, String> hm : headerMap.entrySet()) {
				headers.add(hm.getKey(), hm.getValue());
			}
		}
		StringBuilder paramBuilder = new StringBuilder();
		if (paramQuery != null) {
			if (paramQuery.length != 0) {
				paramBuilder.append("?");
				for (int i = 0; i < paramQuery.length; i++) {
					paramBuilder.append(paramQuery[i].getKey().concat("=".concat(paramQuery[i].getValue())));
					if (i < paramQuery.length - 1) {
						paramBuilder.append("&");
					}
				}
			}
		}
		System.err.println("body : " + new Gson().toJson(body));
		System.err.println("header : " + new Gson().toJson(headers));

		HttpEntity httpEntity = new HttpEntity(body, headers);
		RestTemplate restTemplate = new RestTemplate();
		System.err.println("url yang diberikan : " + url.concat(paramBuilder.toString()));

		String resultApi = new String();
		try {
			System.err.println("masuk ke sistem try : " );
			ResponseEntity<String> responseEntity = 
					restTemplate.exchange(url.concat(paramBuilder.toString()), method,
					httpEntity, String.class);
			System.err.println("masuk get status code : " );
			System.err.println("status : " + responseEntity.getStatusCode());
			System.err.println("result api : " + responseEntity.getBody());
			System.err.println("masuk get status body : " );
			return new HttpRestResponse(responseEntity.getStatusCode(), responseEntity.getBody());
		} catch (Exception exp) {
			if (exp.getMessage().contains("400")) {
				System.err.println("error 400");
				return new HttpRestResponse(HttpStatus.BAD_REQUEST, "User atau Password Salah");
			} else if (exp.getMessage().contains("Connection refused")) {
				System.err.println("Connection refused");
				return new HttpRestResponse(HttpStatus.INTERNAL_SERVER_ERROR,
						"Tidak dapat berkomunikasi dengan service");
			} else {
				System.err.println("exp said: " + exp.getMessage());
				System.err.println("error unidentified");
				return new HttpRestResponse(HttpStatus.NOT_EXTENDED, "Cannot Identified Error Record");
			}
		}
	}
	
	@SuppressWarnings("rawtypes")
	protected HttpRestResponse wsBodyEsbn(String url, Object body, HttpMethod method, Map<String, String> headerMap,
			ParamQueryCustomLib... paramQuery) {
		MultiValueMap<String, Object> header = new LinkedMultiValueMap<>();
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));

		if (headerMap != null) {
			for (Entry<String, String> hm : headerMap.entrySet()) {
				headers.add(hm.getKey(), hm.getValue());
			}
		}
		StringBuilder paramBuilder = new StringBuilder();
		if (paramQuery != null) {
			if (paramQuery.length != 0) {
				paramBuilder.append("?");
				for (int i = 0; i < paramQuery.length; i++) {
					paramBuilder.append(paramQuery[i].getKey().concat("=".concat(paramQuery[i].getValue())));
					if (i < paramQuery.length - 1) {
						paramBuilder.append("&");
					}
				}
			}
		}
		System.err.println("body : " + new Gson().toJson(body));
		System.err.println("header : " + new Gson().toJson(headers));

		HttpEntity httpEntity = new HttpEntity(body, headers);
		RestTemplate restTemplate = new RestTemplate();
		System.err.println("url yang diberikan : " + url.concat(paramBuilder.toString()));

		String resultApi = new String();
		try {
			System.err.println("masuk ke sistem try : " );
			ResponseEntity<String> responseEntity = 
					restTemplate.exchange(url.concat(paramBuilder.toString()), method,
					httpEntity, String.class);
			System.err.println("masuk get status code : " );
			System.err.println("status : " + responseEntity.getStatusCode());
			System.err.println("result api : " + responseEntity.getBody());
			System.err.println("masuk get status body : " );
			return new HttpRestResponse(responseEntity.getStatusCode(), responseEntity.getBody());
		} catch (Exception exp) {
			if (exp.getMessage().contains("400")) {
				System.err.println("error 400");
				return new HttpRestResponse(HttpStatus.BAD_REQUEST, "User atau Password Salah");
			} else if (exp.getMessage().contains("Connection refused")) {
				System.err.println("Connection refused");
				return new HttpRestResponse(HttpStatus.INTERNAL_SERVER_ERROR,
						"Tidak dapat berkomunikasi dengan service");
			} else if(exp.getMessage().contains("409")) {
				System.err.println("Conflict SID");
				return new HttpRestResponse(HttpStatus.CONFLICT,
						"Investor telah terdaftar");
			} else {
				System.err.println("exp said: " + exp.getMessage());
				System.err.println("error unidentified");
				return new HttpRestResponse(HttpStatus.NOT_EXTENDED, "Cannot Identified Error Record");
			}
		}
	}
	
//	@SuppressWarnings("rawtypes")
//	protected <T> HttpResponseClass<T> wsBodyWithRestPoint(String url, Object body, HttpMethod method, Map<String, String> headerMap,
//			ParamQueryCustomLib... paramQuery) {
//		MultiValueMap<String, Object> header = new LinkedMultiValueMap<>();
//		HttpHeaders headers = new HttpHeaders();
//		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
//
//		if (headerMap != null) {
//			for (Entry<String, String> hm : headerMap.entrySet()) {
//				headers.add(hm.getKey(), hm.getValue());
//			}
//		}
//		StringBuilder paramBuilder = new StringBuilder();
//		if (paramQuery != null) {
//			if (paramQuery.length != 0) {
//				paramBuilder.append("?");
//				for (int i = 0; i < paramQuery.length; i++) {
//					paramBuilder.append(paramQuery[i].getKey().concat("=".concat(paramQuery[i].getValue())));
//					if (i < paramQuery.length - 1) {
//						paramBuilder.append("&");
//					}
//				}
//			}
//		}
//		System.err.println("body : " + new Gson().toJson(body));
//		System.err.println("header : " + new Gson().toJson(headers));
//
//		HttpEntity httpEntity = new HttpEntity(body, headers);
//		RestTemplate restTemplate = new RestTemplate();
//		System.err.println("url yang diberikan : " + url.concat(paramBuilder.toString()));
//
//		String resultApi = new String();
//		try {
//			ResponseEntity<InquirySvcGetCustomerAdj> responseEntity = restTemplate.exchange(url.concat(paramBuilder.toString()), method,
//					httpEntity, InquirySvcGetCustomerAdj.class);
//			System.err.println("status : " + responseEntity.getStatusCode());
//			System.err.println("result api : " + responseEntity.getBody());
//			return new HttpResponseClass<T>(responseEntity.getStatusCode(), null);
//		} catch (Exception exp) {
//			if (exp.getMessage().contains("400")) {
//				System.err.println("error 400");
//				return new HttpResponseClass<T>(HttpStatus.BAD_REQUEST, null);
//			} else if (exp.getMessage().contains("Connection refused")) {
//				System.err.println("Connection refused");
//				return new HttpResponseClass<T>(HttpStatus.INTERNAL_SERVER_ERROR, null);
//			} else {
//				System.err.println("exp said: " + exp.getMessage());
//				System.err.println("error unidentified");
//				return  new HttpResponseClass<T>(HttpStatus.NOT_EXTENDED, null);
//			}
//		}
//	}
//	~
}

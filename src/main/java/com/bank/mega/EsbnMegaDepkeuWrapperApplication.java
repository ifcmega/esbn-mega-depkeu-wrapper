package com.bank.mega;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class EsbnMegaDepkeuWrapperApplication {

	public static void main(String[] args) {
		SpringApplication.run(EsbnMegaDepkeuWrapperApplication.class, args);
	}

}
